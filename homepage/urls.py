from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='homepage'),
	path('submit/', views.submit, name='submit'),
#	path('delete/<int:statusModel_id>/', views.deleteItem, name="delete")
	]
	