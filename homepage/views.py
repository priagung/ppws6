from django.shortcuts import render, redirect
from .models import statusModel
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
	status = statusModel.objects.all()
	content = { "statusList" : status }
	return render(request, 'homepage.html' , content)

@csrf_exempt
def submit(request):
		input_status = request.POST.get('status')
		newStatus = statusModel(status = input_status)
		newStatus.save()
		return redirect('homepage:homepage')
	
#def deleteItem(request, statusModel_id):
#    item = statusModel.objects.get(id=statusModel_id)
#    item.delete()
#    return redirect('homepage:homepage')
