from django.test import TestCase
from django.apps import apps
from django.urls import resolve
from django.test.client import Client
from homepage.apps import HomepageConfig
from .views import index
from .models import statusModel


# Create your tests here.
class test(TestCase):
	
    def test_home_page_status_code(self):
            response = self.client.get('/')
            self.assertEquals(response.status_code, 200)
	
    def test_application(self):
            self.assertEqual(HomepageConfig.name, 'homepage')
            self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_correctTemplate(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'homepage.html')

    def test_urlExist(self):
            response = Client().get('/')
            self.assertEqual(response.status_code,200)
			
    def setUp(self):
            new = statusModel.objects.create(status = 'status test create')
            statusCounter = statusModel.objects.all().count()
            self.assertTrue(new.__str__(), new.status)
            self.assertEqual(statusCounter, 1)

    def test_statusDisplay(self):
            status = "displaying status"
            data = {'statusList' : status}
            post_data = Client().post('/', data)
            self.assertEqual(post_data.status_code, 200)
